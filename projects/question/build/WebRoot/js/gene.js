//控制编辑显示
$('table tr, h1, .appraise').live('hover', function(e){
	if(e.type=='mouseover' || e.type=='mouseenter'){
		$(this).find('i').css('display', 'inline-block');
	}else{
		$(this).find('i').hide();
	}
});
$('#reportDialog [who=add]').live('click', function(){
	$('#reportTmpl').tmpl({value : ''}).appendTo($('#reportDialog .modal-body'));
	return false;
});
$('#reportDialog [who=delete]').live('click', function(){
	$(this).parent().parent().remove();
	return false;
});

$('#reportDialog [who=ok]').live('click', function(){
	var array = $('#reportDialog textarea');
	var retVal = [];
	array.each(function(){
		var text = $(this).val();
		if(text){
			retVal.push(text);
		}
	});
	q.report_image = retVal;
	save();
	return false;
});

$('#editReportImage').live('click', function(){
	$('#reportDialog').modal({
		backdrop:true,
		keyboard:true,
		show:true
	});
	
	return false;
});

$('.viewBoolEdit').live('click', function(){
	var parent = $(this).parent();
	var sign = parent.attr('sign');
	var index = parent.parents('[index]').attr('index');
	gene[index][sign] = !gene[index][sign];
	save();
	loadGene();
});

function getGene(key){
	var value = jQuery.extend(true, {}, gene[key]);
	value.index = key;
	value.questionNumbers = '';
	if(value.questions){
		var array = value.questions.split(',');
		var temp = [];
		for(var key in array){
			var t = array[key];
			if(json.questions[t]){
				temp.push(json.questions[t].number);
			}
		}
		value.questionNumbers = temp.join(', ');
	}
	return value;
}

function loadGene(){
	var div = $('#gene').empty();
	if(gene.length==0){
		gene.push({
			key: "all",
			appraise : [],
			calcScript : '',
			questions : '',
			title: "总分",
			viewAppraise: true,
			viewScore: true
		});
	}
	for(var key in gene){
		div.append($('#gene-tmpl').tmpl(getGene(key)));
	}
}


/**
 * 因子编辑框
 * @param content
 * @param callback
 */
function editGengTitleDialog(code, name, callback, title){
	if(!title){
		title = '修改';
	}
	$('#editGeneTitleDialogTitle').text(title);
	$('#editGeneTitleDialogCode').val(code);
	$('#editGeneTitleDialogName').val(name);
	var dialog = $('#editGeneTitleDialog').modal({
		backdrop:true,
		keyboard:true,
		show:true
	});
	var okFunction = null;
	okFunction = function(){
		if(callback){
			var temp = callback($('#editGeneTitleDialogCode').val(), $('#editGeneTitleDialogName').val());
			if(temp != false){
				dialog.modal('hide');
				save();
			}else{
				$('#editGeneTitleDialogOK').one('click', okFunction);
			}
			return false;
		}
	};
	$('#editGeneTitleDialogOK').one('click', okFunction);
}

$(function(){
	$('.geneScriptEdit').live('click', function(){
		var index = $(this).parents('[index]').attr('index');
		textDialog(gene[index].calcScript, function(content){
			gene[index].calcScript = content;
			loadGene();
		},'修改因子计算脚本');
	});

	$('.selectQuestion').live('click', function(){
		var noneTable = $('#selectQuestionDialogTable').empty();
		var hasTable = $('#selectQuestionDialogTableHas').empty();
		var index = $(this).parents('[index]').attr('index');
		var a = [];
		for(var key in json.questions){
			a.push('<tr index="'+key+'"><td>' + json.questions[key].number + '</td><td>'+json.questions[key].title + '</td></tr>');
		}
		var div = $(a.join());
		div.clone().css('color', 'red').appendTo(noneTable);
		
		div.find('tr').hide();
		div.appendTo(hasTable);
		
		$("#selectQuestionDialog").one('shown', function(){
			//隐藏
			hasTable.find('tr').hide();
			if(gene[index].questions){
				var array = gene[index].questions.split(',');
				for(var key in array){
					var value = array[key].trim();
					hasTable.find('tr[index='+value+']').show().attr('has', true);
					noneTable.find('tr[index='+value+']').hide();
				}
			}
		});
		
		var dialog = $('#selectQuestionDialog').modal({
			backdrop:true,
			keyboard:true,
			show:true
		});
		
		$('#selectQuestionOK').one('click', function(){
			var array = [];
			hasTable.find('tr[has=true]').each(function(){
				array.push($(this).attr('index'));
			});
			gene[index].questions = array.join(',');
			loadGene();
			dialog.modal('hide');
			save();
			return false;
		});
	});
	//点击未选中的()
	$('#selectQuestionDialogTable tr').live('click', function(){
		var hasTable = $('#selectQuestionDialogTableHas');
		var index = $(this).hide().attr('index');
		hasTable.find('tr[index='+index+']').show().attr('has', true);
	});
	//点击选中的
	$('#selectQuestionDialogTableHas tr').live('click', function(){
		var noneTable = $('#selectQuestionDialogTable');
		var index = $(this).hide().attr('has', false).attr('index');
		noneTable.find('tr[index='+index+']').show();
	});
	$("#selectQuestionDialog [who=all]").live('click', function(){
		$('#selectQuestionDialogTable tr').trigger('click');
	});
	$("#selectQuestionDialog [who=none]").live('click', function(){
		$('#selectQuestionDialogTableHas tr').trigger('click');
	});
	
	$('.editGeneTitle').live('click', function(){
		var index = parseInt($(this).parents('[index]').attr('index'));
		editGengTitleDialog(gene[index].key, gene[index].title, function(code, name){
			//alert(code + ":" + name);
			gene[index].title = name;
			gene[index].key = code;
			loadGene();
		}, '修改因子');
		return false;
	});
	$('.addGeneTitle').live('click', function(){
		var index = parseInt($(this).parents('[index]').attr('index'));
		editGengTitleDialog('', '', function(code, name){
			var temp = {};
			temp.key = code;
			temp.title = name;
			temp.viewScore = true;
			temp.viewAppraise = true;
			temp.appraise = [];
			gene.splice(index+1, 0, temp);
			loadGene();
		}, '增加因子');
		return false;
	});
	$('.deleteGeneTitle').live('click', function(){
		var index = parseInt($(this).parents('[index]').attr('index'));
		deleteDialog(function(){
			gene.splice(index, 1);
			loadGene();
		});
	});
	$('.geneAppraiseEdit').live('click', function(){
		var index = parseInt($(this).parents('[index]').attr('index'));
		var appraise = gene[index].appraise;
		var div = $('#editGeneAppraiseDialogBody').empty();
		if(appraise){
			for(var key in appraise){
				var value = appraise[key];
				$('#editGeneAppraiseDialog-tmpl').tmpl(value).appendTo(div);
			}
		}
		$('#editGeneAppraiseDialog-tmpl').tmpl().appendTo(div);
		var dialog = $('#editGeneAppraiseDialog').modal({
			backdrop:true,
			keyboard:true,
			show:true
		});
		var okFunction = null;
		okFunction = function (){
			var array = [];
			$('#editGeneAppraiseDialogBody div').each(function(){
				var temp = {};
				try{
					var div = $(this).children(":eq(0)");
					var min = parseFloat(div.children(":eq(0)").val());
					var max = parseFloat(div.children(":eq(1)").val());
					if(isNaN(min) && isNaN(max)){
						return;
					}else{
						temp.minScore = min;
						temp.maxScore = max;
					}
				}catch (e) {
					alert('分数必须为数字');
					$('#editGeneAppraiseDialogOK').one('click', okFunction);
					return ;
				}
				temp.content = $(this).children(":eq(1)").val();
				temp.suggestion = $(this).children(":eq(2)").val();
				array.push(temp);
			});
			//排序
			gene[index].appraise = array;//.sort(function(x,y){return parseInt(x.maxScore)-parseInt(y.maxScore);});
			loadGene();
			dialog.modal('hide');
			save();
			return false;
		};
		$('#editGeneAppraiseDialogOK').one('click', okFunction);
	});
	$('.geneAppraiseAdd').live('click', function(){
		var div = $(this).parent().parent();
		div.after($('#editGeneAppraiseDialog-tmpl').tmpl());
	});
	$('.geneAppraiseAdd2').live('click', function(){
		$('#editGeneAppraiseDialog-tmpl').tmpl().prependTo($('#editGeneAppraiseDialogBody'));
	});
	$('#editWarnDialog [data-who=add]').live('click', function(){
		var div = $('#editWarnDialog [data-who=body]');
		$('#editWarnDialog-tmpl').tmpl().appendTo(div);
	});
	
	var warnCallback = null;
	$('#editWarnDialog [data-who=ok]').live('click', function(){
		var a = [];
		var form = $('#editWarnDialog .dialogChoice');
		for(var i=0; i<form.length; i++){
			var f = $(form[i]);
			var o = {};
			o.max = parseFloat(f.find('[data-form=max]').val());
			o.min = parseFloat(f.find('[data-form=min]').val());
			o.content = f.find('[data-form=content]').val();
			if(isNaN(o.max) || isNaN(o.min)){
				
			}else{
				a.push(o);
			}
		}
		if(warnCallback){
			warnCallback(a);
		}
	});
	$('#gene [data-who=warn]').live('click', function(){
		var key = $(this).attr('data-key');
		var div = $('#editWarnDialog [data-who=body]').empty();
		var a = [];
		if(q.warn && q.warn[key] && q.warn[key].length){
			a = $.extend([], q.warn[key]);
		}
		a.push({});
		for(var i=0; i<a.length; i++){
			var t = a[i];
			$('#editWarnDialog-tmpl').tmpl(t).appendTo(div);
		}
		var dialog = $('#editWarnDialog').modal({
			backdrop:true,
			keyboard:true,
			show:true
		});
		warnCallback = function(a){
			if(!q.warn){
				q.warn = {};
			}
			q.warn[key] = a;
			save();
			loadGene();
			dialog.modal('hide');
		};
		return false;
	});

	
	$('.geneUp').live('click', function(){
		var index = parseInt($(this).parents('[index]').attr('index'));
		if(index==0){
			return;
		}
		var temp = gene[index];
		gene[index] = gene[index-1];
		gene[index-1] = temp;
		loadGene();
		save();
	});
	
	$('.geneDown').live('click', function(){
		var index = parseInt($(this).parents('[index]').attr('index'));
		if(index==gene.length-1){
			return;
		}
		var temp = gene[index];
		gene[index] = gene[index+1];
		gene[index+1] = temp;
		loadGene();
		save();
	});
	
});
