$(function(){
	var menuHide = function(){
		$(".navbar").trigger('click');
	};
	
	$('#exportQuestion').click(function(){
		menuHide();
		
		return false;
	});
	$('#importQuestion').click(function(){
		menuHide();
		
		return false;
	});
	$('#autoQuestionNumber').click(function(){
		menuHide();
		for(var key in json.questions){
			json.questions[key].number = parseInt(key)+1;
		}
		save();
		load();
		return false;
	});
	$('#autoAnswer').click(function(){
		menuHide();
		$('#con .test_panel').each(function(){
			var as = $(this).find('a');
			var index = parseInt(as.length*Math.random());
			$(as.get(index)).trigger('click');
		});
		return false;
	});
	$('#seeReport').click(function(){
		localStorage.setItem("previewId", questionId);
		//document.getElementById('uploadfile').click();
	});
	$('#uploadfile').change(function(){
		
	});
	$('#previewList').click(function(){
		localStorage.setItem("previewId", questionId);
	});
	//保存答案
	$('#savaAnswer').click(function(){
		if(document.title.search('\\*') != -1){
			alert('请在量表提交后，再保存答案');
			return ;
		}
		textDialog('', function(remark){
			if(!remark){
				alert('必须填写答案的目的');
				return;
			}
			$.ajax({
				'type': 'post',
				'url': "/question/saveAnswer.json",
				'data': {
					'a.typical_customer_id' : customer.id,
					'a.question_id' : q.id,
					'a.version' : q.version,
					'a.answer' : localStorage.getItem(q.id + '_answer'),
					'a.intro' : remark
				}, 
				'dataType' : 'json',
				'async' : true,
				'success' : function(json) {
					//$$.go(contextPath + '/question/preview');//跳转至预览
					q.version = json.version;
					save();
				},
				'error' : function(e){
					for(var k in e){
						console.log(k);
						console.log(e[k]);
						console.log("----------------------");
					}
					alert('提交失败，请稍后重试或与管理员联系！');
				}
				
			});
		}, '提交答案至服务器，请简要介绍此答案的目的', '请简要介绍此答案的目的');
	});
	
	//选择典型用户
	var edus = {"UNDERPRIMARY":"小学以下", "PRIMARY":"小学", "JUNIOR":"中学", "SENIOR":"高中", "SECONDARY":"中专", 
			"COLLEGE":"大专", "UNDERGRADUATE":"本科", "MASTER":"硕士", "DOCTOR":"博士", "POSTDOCTORAL":"博士后"};
	var eduArray = ["UNDERPRIMARY", "PRIMARY", "JUNIOR", "SENIOR", "SECONDARY", "COLLEGE", "UNDERGRADUATE", "MASTER", "DOCTOR", "POSTDOCTORAL"];
	var select = $('#selectTypicalCustomerDialog select');
	for(var i=0; i<eduArray.length; i++){
		var t = eduArray[i];
		select.append('<option value="' + t + '">' + edus[t] + '</option>');
	}
	var d = null;
	select.change(function(){
		customer.edu = select.val();
		localStorage.setItem('Customer', JSON.stringify(customer));
		d.modal('hide');
	}); 
	
	$('#typicalCustomer').click(function(){
		var parent = $(this);
		 d = $('#selectTypicalCustomerDialog').modal({
			backdrop:true,
			keyboard:true,
			show:true
		});
		var tmpl = function (str,data) {
			return str.replace(/\${(.*?)}/igm,function($,$1) {
				return data[$1]?data[$1]:$;
			});
		};
		select.val(customer.edu);
		var table = d.find('table').empty();
		$.getJSON("/question/all.json", function(json){
			if(json.code){
				
			}else{
				var now = Customer();
				now = now ? now : {};
				for(var a in json.data){
					(function(data){
						var temp = tmpl('<tr><td>${name}</td><td>${age}</td><td>${gender}</td></tr>', data);
						temp = $(temp);
						if(data.id == now.id){
							temp.css('color', '#D14');
						}
						temp.appendTo(table);
						temp.one('click', function(){
							data.edu = select.val();
							localStorage.setItem('Customer', JSON.stringify(data));
							customer = data;
							parent.html('测试典型：' + data.name);
							d.modal('hide');
						});
					})(json.data[a]);
				}
			}
		});
		return false;
	}).html('测试典型：'+customer.name);
});