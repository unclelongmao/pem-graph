//============jQuery.tmpl 设置变量=================
(function($) {
	var table = {};
	$.extend({
		setVar:function(key, value){
			table[key] = value;
			return "";
		},
		getVar:function(key){
			return table[key];
		},
		removeVar : function(key){
			delete table[key];
			return "";
		},
		removeAllVar : function (){
			for(var key in table){
				delete table[key];
			}
			return "";
		}
	});
})(jQuery);
// =======================Base64===================
var Base64 = (function(){
	var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_*";
	var _utf8_encode = function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
		for (var n = 0; n < string.length; n++) {
			var c = string.charCodeAt(n);
			if (c < 128) {
				utftext += String.fromCharCode(c);
			} else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			} else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
 
		}
		return utftext;
	};
	var _utf8_decode = function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
		while ( i < utftext.length ) {
			c = utftext.charCodeAt(i);
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			} else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			} else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
		}
		return string;
	};
	return {
		encode : function (input) {
			var output = "";
			var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
			var i = 0;
			input = _utf8_encode(input);
			while (i < input.length) {
				chr1 = input.charCodeAt(i++);
				chr2 = input.charCodeAt(i++);
				chr3 = input.charCodeAt(i++);
				enc1 = chr1 >> 2;
				enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
				enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
				enc4 = chr3 & 63;
				if (isNaN(chr2)) {
					enc3 = enc4 = 64;
				} else if (isNaN(chr3)) {
					enc4 = 64;
				}
				output = output +
				_keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
				_keyStr.charAt(enc3) + _keyStr.charAt(enc4);
			}
			return output;
		},
		decode : function (input) {
			var output = "";
			var chr1, chr2, chr3;
			var enc1, enc2, enc3, enc4;
			var i = 0;
			//input = input.replace(/[^A-Za-z0-9\+\/\=]/g, ""); 不用校验
			while (i < input.length) {
				enc1 = _keyStr.indexOf(input.charAt(i++));
				enc2 = _keyStr.indexOf(input.charAt(i++));
				enc3 = _keyStr.indexOf(input.charAt(i++));
				enc4 = _keyStr.indexOf(input.charAt(i++));
				chr1 = (enc1 << 2) | (enc2 >> 4);
				chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
				chr3 = ((enc3 & 3) << 6) | enc4;
				output = output + String.fromCharCode(chr1);
				if (enc3 != 64) {
					output = output + String.fromCharCode(chr2);
				}
				if (enc4 != 64) {
					output = output + String.fromCharCode(chr3);
				}
			}
			output = _utf8_decode(output);
			return output;
		}
	};
})();

//=======================smart Tools===================
(function(window, $) {
    var document = window.document, location = window.location;//document = window.document,navigator = window.navigator,
    var callbacks = [];//处理注删多个回调
    var jSmart = (function(){
        var s = function(a){
            return new s.fn.init(a);
        };
        s.fn = s.prototype = {
            init:function(a){
            	if(typeof a=="function"){
            		s.ready(a);
            	}
            	if(typeof a=="string"){
            		return $(document.getElementById(a));//包装 id 包含 .
            	}
                return this;
            },
            author:'Gaollg',
            version:'1.0.0'
        };
        
        s.ready = function(a){
        	callbacks.push(a);
        };
        
        s.ajax = function(url, params, success, error){
        	$.ajax({
        		'type': 'post',
        		'url': url,
        		'data': params, 
        		'dataType': 'json',
        		'async' : false,
        		'success' : function(json) {
        			//处理formError
        			
        		}
        	});
        };
        
        /**
    	 * 取得动态参数值
    	 */
        s.getQueryString = function(name) {
    		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    		var r = window.location.search.substr(1).match(reg);
    		if (r != null)
    			return unescape(r[2]);
    		return null;
    	};
    	/**
    	 * 取得所有动态参数值
    	 * @returns {Object}
    	 */
    	s.getAllQuery = function () {
    		   var url = location.search; //获取url中"?"符后的字串
    		   var theRequest = new Object();
    		   if (url.indexOf("?") != -1) {
    		      var str = url.substr(1);
    		      strs = str.split("&");
    		      for(var i = 0; i < strs.length; i ++) {
    		         theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
    		      }
    		   }
    		   return theRequest;
    	};
        
        /**
         * 跳转路径 
         * url
         * toThis
         *     true 表示来源页更改为本页面
         *     false 表示清除来源页参数
         *     不传递或null 表示保持
         */
        s.go = function(url, toThis){
        	var referer = '';
        	if(toThis == true){
        		//跳转到本页
        		referer = location.href;
        	}
        	else if(toThis == false){
        		referer = null;
        	}else{
        		referer = s.getReferer();
        	}
        	if(referer){
        		if(url.indexOf('?') == -1){
        			url = url + '?';
        		}else{
        			url = url + '&';
        		}
        		url = url + 'smart_referer=' + Base64.encode(referer);
        	}
			location.href = url;
        };
        //取得来源页
        s.getReferer = function(){
			var a = s.getQueryString('smart_referer');
			if(a){
				return Base64.decode(a);
			};
		};
		
        //return false 表示未作处理
		s.hanldError = function(json){
			if(smartCode && smartCode[json.code]){
				var temp = smartCode[json.code];
				if(temp && temp.url){
					s.go(temp.url, true);
					return true;
				}
			}
			return false;
		};
		
        s._callbackInvoke = function(json){
        	if(s.hanldError(json)){
        		return;
        	}
        	//if(!handleJson(json)){
        	for(var k in callbacks){
        		callbacks[k](json);
        	}
        	//}
        };
        s.fn.init.prototype = s.fn; // 使用xQuery的原型对象覆盖init的原型对象
        return s;
    })();
    
    window.jSmart = window.$$ = jSmart;
})(window, jQuery);

//============================ 表单=============================
(function(window, $){
	var getArray = function(a){
		a = a + "@@@@@@";
		var b = 0;
		var array = [];
		for(var i=0; i<a.length; i++){
			var c = a.charAt(i);
			if(c=='\\'){
				i++;
				continue;
			}
			if(c=='"' || c=="'"){
				i = a.indexOf(c, i+1);
				continue;
			}
			if(c == '@'){
				array.push(a.substring(b, i).trim());
				b = i+1;
			}
		}
		return array;
	};
	
	var getPrefixId = function($this, name){
		return $this.formId.replace(/\./g, '_') + "_" + name.replace(/\./g, '_');
	};
	
	var vAdd = function(input, setting){//必须为jQuery对象
		var $this = this;
		var form = this.form;
		var name = input.attr('name');
		var id = getPrefixId(this, name);
		
		input.attr('id', id);
		form.find('label[for="' + name+'"]').attr('for', id);
		var label = form.find('label[for="' + id+'"]').attr('id', id + '_label');
		var v = input.attr('v');
		if(!v){//无校验 返回
			return ;
		}
		
		//取得不同样式的
		var temp = {};
		temp.tips = $.extend(this.tips, setting);
		temp.label = label;
		temp.input = input;
		
		var a = getArray(v);
		temp.express = a[0];
		temp.errorMsg = a[1];
		temp.focusMsg = a[2];
		temp.showMsg = a[3];//label 请用placeholder 参考:http://www.oschina.net/question/5189_22929
		temp.correctMsg = a[4];
		
		this.members[name] = temp;
		//监听事件
		input.focus(function(){
			temp.input.removeClass(temp.tips.onShowClass)
				.removeClass(temp.tips.onErrorClass)
				.removeClass(temp.tips.onCorrectClass)
				.addClass(temp.tips.onFocusClass);
			temp.label.html(temp.tips.onFocusHtml.replace(/\$/g, temp.focusMsg));
		});
		input.blur(function(){
			$this.vHandle(name);
		});
		
		//显示默认提示
		temp.label.addClass(temp.tips.onShowClass);
		temp.label.html(temp.tips.onShowHtml.replace(/\$/g, temp.showMsg));
		
		//增加默认提示
		var v = function(regex, data){
			var t = jQuery.vForm(regex);
			if(t){
				if(!temp.errorMsg && t.error){
					temp.errorMsg = t.error;
				}
				if(!temp.focusMsg && t.focus){
					temp.focusMsg = t.focus;
				}
				if(!temp.showMsg && t.show){
					temp.showMsg = t.show;
				}
			}
		};
		eval(temp.express);
		return this;
	};
	var vHandle = function(name){//校验整个表单
		if(name){
			//移除样式
			var temp = this.members[name];
			temp.input.removeClass(temp.tips.onShowClass)
			.removeClass(temp.tips.onErrorClass)
			.removeClass(temp.tips.onCorrectClass)
			.removeClass(temp.tips.onFocusClass);
			//增加样式
			var $ = [];
			var type = temp.input.get(0).type;
			switch(type){
				case "text":
				case "hidden":
				case "password":
				case "textarea":
				case "file":
				case "radio":
				case "select-one":
					$ = temp.input.val();
					break;
				case "checkbox":
					temp.input.filter(':checked').each(function(){
						$.push($(this).val());
					});
					break;
				case "select-multiple":
					temp.input.find(':selected').each(function(){
						$.push($(this).val());
					});
					break;
			};
			
			var clazz = '', msg = '', html = '';
			var v = function(regex, data){
				var t = jQuery.vForm(regex);
				if(t){
					if(t.regex){
						regex = t.regex;
					}
					if(t.fun){
						return t.fun(data);
					}
				}
				// 执行 
				return (new RegExp(regex, 'i')).test(data);
			};
			var retVal = eval(temp.express);
			
			if(retVal){//通过
				clazz = temp.tips.onCorrectClass;
				html = temp.tips.onCorrectHtml;
				msg = temp.correctMsg;
				retVal = true;
			}else{
				clazz = temp.tips.onErrorClass;
				html = temp.tips.onErrorHtml;
				if(temp.errorMsg){
					msg = temp.errorMsg;
				}
				retVal = temp.errorMsg;
			}
			temp.input.addClass(clazz);
			temp.label.html(html.replace(/\$/g, msg));
			//校验单个
			return retVal;
		}
		//校验所有
		var errors = [];
		for(var key in this.members){
			var retVal = this.vHandle(key);
			if(retVal != true){
				if(errors.length == 0){
					this.members[key].input.focus();
				}
				errors.push(retVal);
			}
		}
		
		if(errors.length){
			if(this.errorLabel){
				this.errorLabel.html(errors[0]);
			}
			return errors;
		}
		if(this.errorLabel){
			this.errorLabel.html('');
		}
		return true;//正确
	};
	
	//扩展
	$.extend($.fn, {
		vInit : function(setting){
			var v = $.extend({
				form : $(this),
				tips : {
					onShowHtml : "<font color='#48484C'>$</font>",
					onFocusHtml : "<font color='#1E347B'>$</font>",
					onErrorHtml : "<font color='#D14'>$</font>",
					onCorrectHtml : "<font color='teal'>$</font>",
					onShowClass : "",
					onFocusClass : "",
					onErrorClass : "",
					onCorrectClass : ""
				},
				errorLabel : null,
				error : function(){//提交失败时回调
				},
				success : function(){//成功返回数据
				},
				vfail : function(){//校验时错误回调
				},
				members : {},
				ajaxV : false,
				action : $(this).get(0).action
			}, setting);
			v.vAdd = vAdd;
			v.vHandle = vHandle;
			v.formId = v.form.attr('id');
			//提交时校验
			v.form.get(0).action = v.action;
			var count = 0;
			var data = "";
			v.form.submit(function(){
				//校验每个元素
				var retVal = v.vHandle();
				if(retVal == true){
					var temp = v.form.serialize();
					if(temp != data){
						count = 0;
						data = temp;
					}
					count ++;
					if(count > 3){
						alert('不用那么累了,点一个就够了:-)');
						return false;
					}else if(count!=1){
						return false;
					}
					//清空错误信息
					v.form.find(":input[name]").each(function(){
						$('#' + getPrefixId(v, $(this).attr('name')) + "_label").html('');
					});
					//ajax提交...
		        	$.ajax({
		        		'type': 'post',
		        		'url': v.form.get(0).action,
		        		'data': data, 
		        		'dataType': 'json',
		        		'success' : function(json) {
		        			//处理
		        			if(json.code==-2){
		        				for(var k in json.errors){
		        					var value = json.errors[k];
		        					$('#' + getPrefixId(v, k) + "_label").html(v.tips.onErrorHtml.replace(/\$/g, value));
		        				}
		        			}else if(json.code==0){
		        				v.success(json);
		        			}else{
		        				v.error(json);
		        			}
		        			count = 0;
		        			if(v.errorLabel){
		        				v.errorLabel.html(json.msg);
		        			}
		        		},
		        		'error' : function(){
		        			count = 0;
		        			var json = {code:0,msg:"ajax Error"};
		        			v.error(json);
		        			if(v.errorLabel){
		        				v.errorLabel.html(json.msg);
		        			}
		        		}
		        	});
				}else{
					v.vfail(retVal);
				}
				return false;
			});
			//遍历Form
			v.form.find(":input[name]").each(function(){
				v.vAdd($(this));
			});
			if(v.ajaxV){
				$.ajax({
					'type': 'post',
					'url': v.form.get(0).action,
					'data': {
						'...autov...' : '...autov...' //我就不信 还有会弄这样的参数
					}, 
					'dataType': 'json',
					'async' : false,
					'success' : function(json) {
						//处理formError
						if(json.code!=0){
							return ;
						}
						//
						var handle = function(roules){
							if(!roules || roules.length==0){
								return;
							}
							for(var k in roules){
								var str = roules[k] + "@@@@@@@@@";
								var i = str.indexOf('@');
								var name = str.substring(0, i).trim();
								var value = str.substring(i+1, str.length);
								var dom = v.form.find(":input[name='" + name + "']");
								if(dom.length && !dom.attr('v')){
									dom.attr('v', value);
									v.vAdd(dom);
								}
							}
						};
						handle(json.methodRules);
						handle(json.clazzRules);
					}
				});
			}
			return v;
		},
		vFillForm : function(json){
			$(this).find(":input[name]").each(function(){
				var type = this.type;
				var $this = $(this);
				var name = $this.attr('name');
				var value = (function(){
					var array = name.split('\.');
					var t = json;
					for(var i in array){
						if(t){
							t = t[array[i]];
						}else{
							return ;
						}
					}
					return t;
				})();
				switch(type){
					case "text":
					case "hidden":
					case "password":
					case "textarea":
					case "file":
					case "radio":
					case "select-one":
						$this.val(value);
						break;
					case "checkbox"://TODO 
						temp.input.filter(':checked').each(function(){
							$.push($(this).val());
						});
						break;
					case "select-multiple"://TODO 
						temp.input.find(':selected').each(function(){
							$.push($(this).val());
						});
						break;
				};
			});
		}
	});
	$.vForm = function(add){
		if((typeof add) == "string"){
			return $.vForm.regex[add];
		}else{
			$.vForm.regex = $.extend($.vForm.regex, add);
		}
	};
	if(!$.vForm.regex){
		$.vForm.regex = {};
	}
})(window, jQuery);

function callback(json){
	jSmart._callbackInvoke(json);
};
