//保存kind
function saveKind() {
	var id = $('#kindId').val();
	var name = $('#kindName').val();
	
	if(id=="")
	{
		alert("id不能为空");
		return ;
	}
	
	if(name=="")
	{
		alert("类别名称不能为空");
		return;
	}
	$.ajax({
		type : 'post',
		url : '/kind/addKind.json',
		data : {
			'id' : id,
			'kindName' : name,
		},
		dataType : 'json',
		success : function(result) {
			if (result.msg == "success") {
				location.reload();

			} else {
				alert("失败");

			}
		}
	});

}
// 控制问题编辑显示
$('table tr, h1, .edit').live('hover', function(e) {
	if (e.type == 'mouseover' || e.type == 'mouseenter') {
		$(this).find('i').css('display', 'inline-block');
	} else {
		$(this).find('i').hide();
	}
});
// 解析方法 绑定模板
$$(function(json) {
	if (json.code != 0) {
		alert('code: ' + json.code + ',msg:' + json.msg);
		return;
	}
	for ( var key in json.data) {
		var value = json.data[key];
		$('#con-tmpl').tmpl(value).appendTo('#con')
	}
});
//设置数据
function setDate(id, kindName) {
	$("#updateId").val(id);
	$("#updateKindName").val(kindName);

}

function setQDate(id) {
	$("#kind_id").val(id);

}
//更新类别
function update() {
	var id = $('#updateId').val();
	var name = $('#updateKindName').val();
	
	if(name=="")
	{
		alert("类别名称不能为空");
		return;
	}
	
	$.ajax({
		type : 'post',
		url : '/kind/update.json',
		data : {
			'id' : id,
			'kindName' : name,
		},
		dataType : 'json',
		success : function(result) {
			if (result.msg == "success") {
				location.reload();

			} else {
				alert("失败");

			}
		}
	});
}
//添加问题
function addQuestion() {
	var id = $('#question_id').val();
	var kindId = $('#kind_id').val();
	var name = $('#question_name').val();
	
	if(id=="")
	{
		alert("量表id不能为空");
		return ;
	}
	
	if(name=="")
	{
		alert("量表名称不能为空");
		return;
	}
	
	
	$.ajax({
		type : 'post',
		url : '/kind/addQuestion.json',
		data : {
			'id' : id,
			'kindId' : kindId,
			'questionName' : name,
		},
		dataType : 'json',
		success : function(result) {
			if (result.msg == "success") {
				//location.href = "/question/edit/" + id;
				window.location.reload();
			} else {
				alert("失败");

			}
		}
	});

}
