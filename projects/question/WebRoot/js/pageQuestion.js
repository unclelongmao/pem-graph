//使用到了本地存储,如果不支持,请更换浏览器
if (!window.localStorage) {
    alert('不支持本浏览器,请使用Chrome,Firefox');
    window.close();
}
var json = null;
var calc = {};
var gene = {};
var answer = [];
var isReload = false;
var q = {};

function save() {
    var str = JSON.stringify(q);
    console.log(str)
    if (onLineQuestion == str) {
        document.title = "量表编辑";
    } else {
        document.title = "* 量表编辑";
    }

    localStorage.setItem(q.id, str);
}

function read(id) {
    return localStorage.getItem(id);
}

function saveAnswer(index, checked) {
    if (!isReload) { //这是自动填充答案
        return;
    }
    answer[index] = checked;
    localStorage.setItem(q.id + '_answer', JSON.stringify(answer));
    geneScoreView();
}

function geneScoreView() {
    //计算因子分数
    var genescoreArray = null;
    try {
        var scoreArray = calcQuestionScore(calc, answer, customer);
        genescoreArray = calcGeneScore(gene, scoreArray, customer);
    } catch (e) {
        console.log(e);
        //if(!e.search(" no answer.")){
        //	alert(e);
        //}
    }
    //显示分数
    var a = [];
    for (var key in gene) {
        var value = getGene(key);
        a.push("<tr title='");
        a.push(value.title + " : " + value.questionNumbers);
        a.push("'><td>");
        a.push(value.key);
        a.push("</td><td>");
        if (genescoreArray && genescoreArray[key]) {
            a.push(genescoreArray[key].score);
        } else {
            a.push("--");
        }
        a.push("</td>");
    }
    $(a.join('')).appendTo($('#geneScoreView').empty());
    if (genescoreArray) {
        $('#savaAnswer').parent().show();
    } else {
        $('#savaAnswer').parent().hide();
    }
}

function readAnswer() {
    var temp = localStorage.getItem(q.id + '_answer');
    if (temp) {
        return string2Json(temp);
    }
    return [];
}

function string2Json(str) {
    return eval('(' + str + ')');
}

//jQuery.tmpl 设置变量
var gaollg = {
    set: function(key, value) {
        if (!gaollg.table) {
            gaollg.table = {};
        }
        gaollg[key] = value;
        return "";
    },
    get: function(key) {
        return gaollg[key];
    }
};
/**
 * 删除对话框 return false表示不关闭对话框
 * @param callback 回调函数
 */
function deleteDialog(callback) {
    var dialog = $('#deleteQuestionDialog').modal({
        backdrop: true,
        keyboard: true,
        show: true
    });
    $('#deleteQuestionOK').one('click', function() {
        if (callback) {
            var temp = callback();
            if (temp != false) {
                dialog.modal('hide');
                save();
            }
        }
        return false;
    });
};

/**
 * 编缉问题对话框
 * @param dataP
 * @param okcallback
 * @param showncallback
 */
function editDialog(dataP, okcallback, showncallback) {
    $("#editQuestionDialog").empty().append($("#editQuestionDialog-tmpl").tmpl(dataP));
    $("#editQuestionDialog").on('shown', function() {
        var html = [];
        html.push('<option selected value="">不使用</option>');
        for (var key in json.commChoices) {
            html.push('<option value="' + key + '">' + key + '</option>');
        }
        $('#commChoiceSelect').empty().append($(html.join('')));
        $('#' + dataP.type + "Label").attr("checked", 'checked');
        if (showncallback) {
            showncallback();
        }
    });
    var dialog = $('#editQuestionDialog').modal({
        backdrop: true,
        keyboard: true,
        show: true
    });
    var okFunction = null;
    okFunction = function() {
        if (okcallback) {
            var data = {};
            var score = {};
            data.title = $('#editDialogHead input:eq(1)').val();
            data.beforeScript = $('#groupDialog [data-type=beforeScript]').val();
            data.changeAnswer = $('#groupDialog [data-type=changeAnswerScript]').val();
            data.autoFillAnswer = $('#groupDialog [data-type=autoFillAnswer]').val();

            if (dataP.isComm) {
                data.commKey = $('#editDialogHead input:eq(0)').val();
            } else {
                data.number = $('#editDialogHead input:eq(0)').val();
            }

            var comm = $('#commChoiceSelect').val();
            score.beforeScript = data.beforeScript;
            if (comm) {
                data.commChoice = comm;
                score.commChoice = comm;
            } else {
                //取得公共选项
                data.type = $('#choiceType').find('[name=choiceType]:checked').val();
                data.choiceGroup = [];
                score.choices = {};
                score.type = data.type;
                score.calcScript = $('#groupDialog [data-type=calcScript]').val();
                score.autoFillAnswer = $('#groupDialog [data-type=autoFillAnswer]').val();
                $('#groupDialog .choiceGroup').each(function() {
                    var group = {};
                    var temp = $(this).find('div:eq(0) input').val();
                    if (temp) {
                        group.title = temp;
                    }
                    group.choices = {};
                    $(this).find('.dialogChoice').each(function() {
                        var temp = $(this).children('input:eq(0)').val();
                        group.choices[temp] = {};
                        group.choices[temp].title = $(this).children('input:eq(1)').val();
                        score.choices[temp] = $(this).children('input:eq(2)').val();
                    });
                    data.choiceGroup.push(group);
                });
            }

            //console.log(JSON.stringify(data));
            //console.log(JSON.stringify(score));
            var temp = okcallback(data, score);
            if (temp != false) {
                dialog.modal('hide');
                //重新设置引用
                load(json);
                save();
            } else {
                $('#editDialogOK').one('click', okFunction);
            }
        }
    };
    $('#editDialogOK').one('click', okFunction);
};

/**
 * 文本编辑框
 * @param content
 * @param callback
 */
function textDialog(content, callback, title) {
    if (!title) {
        title = '修改';
    }
    $('#editTextDialogTitle').text(title);
    $('#textDialogValue').val(content);
    var d = $('#editTextDialog').modal({
        backdrop: true,
        keyboard: true,
        show: true
    });

    var func = function() {
        if (callback) {
            callback($('#textDialogValue').val());
            save();
        }
    };
    $('#editTextDialogOK').one('click', func);
    d.on('hide', function() {
        $('#editTextDialogOK').unbind();
    });
}

$(function() {
    $('textarea').live('focus', function() {
        var $this = $(this);
        $this.typeahead({
            source: ['max();', 'min();', 'sum();', 'avg();', 'count();'],
            matcher: function(a) {
                return $this.val().length < 2;
            }
        });
    });

    $('#switchMain').click(function() {
        var $this = $(this);
        if ($this.attr('now') == 'question') {
            $this.attr('now', 'gene').text('查看因子');
            $('#question').show();
            $('#gene').hide();
            geneScoreView();
            location.hash = '#editQuestion';
        } else {
            $this.attr('now', 'question').text('条目编辑');
            $('#question').hide();
            $('#gene').show();
            location.hash = '#editGene';
            loadGene();
        }
        return false;
    });
    if (location.hash == '#editGene') {
        $('#switchMain').trigger('click');
    }
    //控制题目显示 [编辑]
    $('.test_panel').live('hover', function(e) {
        if (e.type == 'mouseover' || e.type == 'mouseenter') {
            $(this).find('h3 i').show();
        } else {
            $(this).find('h3 i').hide();
        }
    });
    //控制问题编辑显示
    $('.dialogChoice').live('hover', function(e) {
        if (e.type == 'mouseover' || e.type == 'mouseenter') {
            $(this).find('i').css('display', 'inline-block');
        } else {
            $(this).find('i').hide();
        }
    });

    //答题 (选择题)
    $('.test_panel > a').live('click', function() {
        var group = $(this).attr('group');
        var checked = {};
        var autoFillAnswer = $(this).attr("data-autoFillAnswer");


        if ($(this).hasClass("test_selected")) {
            if ($(this).siblings('.test_selected').length == 0) {
                return;
            }
            $(this).removeClass('test_selected');
        } else {
            $(this).addClass('test_selected').siblings('[group!=' + group + ']').removeClass('test_selected');
            checked[$(this).attr('value')] = parseFloat($(this).attr('score'));
        }
        //计算得分
        $(this).siblings('.test_selected').each(function() {
            checked[$(this).attr('value')] = parseFloat($(this).attr('score'));
        });
        var index = parseInt($(this).parents('[index]').attr('index'));
        var script = $(this).parents('[calcScript]').attr('calcScript');
        try {
            var score = calcScore(checked, script);
            //console.log(JSON.stringify(checked) + ' | ' + script);
            $(this).siblings('h3').children('score').html(score + "分");
            var t = [];
            for (var key in checked) {
                t.push(key);
            }
            saveAnswer(index, t);
        } catch (e) {
            $(this).siblings('h3').children('score').html("--分");
        }


        if (autoFillAnswer !== "false") {
            autoFillAnswer = JSON.parse(autoFillAnswer)
            $.each(autoFillAnswer, function(index, val) {
                console.log(val)
                var $elm = $(".test_panel[number='" + index + "']")
                    .find("a[value='" + val + "']")
                var $body = $("body")
                $body.scrollTop($elm.offset().top - 120)
                $elm.trigger("click")

            })
        }

    });

    //编辑文本
    $('.textEdit').live('click', function() {
        var div = $(this).parent().next('[who]');
        textDialog(div.text(), function(content) {
            div.text(content);
            var temp = div.attr('who');
            q[temp] = content;
        }, $(this).attr('title'));
    });
    $('#questionTitleEdit').live('click', function() {
        var div = $('#questionTitle');
        textDialog(div.text(), function(content) {
            div.text(content);
            q[div.attr('who')] = content;
        }, '修改量表名称');
    });

    //问题编辑
    $('.questionEdit').live('click', function() {
        var index = parseFloat($(this).parent().parent().attr('index'));
        var data = getQuestion(index);
        if (data.commChoice) {
            data.choiceGroup = [];
        }
        editDialog(data, function(data, _score) {
            //保存
            //alert('保存');
            setQuestion(index, data);
            //保存分数
            setQuestionScoreJson(index, _score);
        }, function() {
            $('#commChoiceSelect').val(data.commChoice);
        });
    });

    //公共选项编辑
    $('.commChoiceEdit').live('click', function() {
        var div = $(this).parent().parent();
        var index = div.attr('index');
        editDialog(getCommChoiceJson(index), function(data, _score) {
            //保存
            var temp = data.commKey;
            if (temp) {
                delete data.commKey;
                //校验是否有使用
                if (temp != index) {
                    //检测是否与其他公共属性重名
                    if (isComm(temp)) {
                        alert(temp + ' 选项已存在');
                        return false;
                    }
                    if (useingComm(index)) {
                        alert('公共选项:' + index + " 已被使用,无法更改其称.");
                        temp = index;
                        return false;
                    }
                }
                setCommChoiceJson(temp, data, index);
                setCommScoreJson(temp, _score, index);
                //div.replaceWith($("#question-tmpl").tmpl(getCommChoiceJson(temp))); //为了性能
            }
        });

    });

    $('.commChoiceAdd').live('click', function() {
        var data = {};
        data.isComm = true;
        data.score = {};
        editDialog(data, function(data, _score) {
            var temp = data.commKey;
            if (temp) {
                delete data.commKey;
                //检测是否与其他公共属性重名
                if (isComm(temp)) {
                    alert(temp + ' 选项已存在');
                    return false;
                }
                setCommChoiceJson(temp, data);
                setCommScoreJson(temp, _score);
            }
        });
    });

    $('.questionAdd').live('click', function() {
        var div = $(this).parent().parent();
        var data = {};
        data.isComm = false;
        editDialog(data, function(data, _score) {
            var index = 0; //默认插在首个
            if (div.parent().attr('id') == 'con') {
                index = parseFloat(div.attr('index')) + 1;
            }
            json.questions.splice(index, 0, data);
            //保存分数
            calc.question.splice(index, 0, _score);
        });
    });

    $('.commChoiceDelete').live('click', function() {
        var div = $(this).parent().parent();
        deleteDialog(function() {
            var index = div.attr('index');
            div.remove();
            deleteCommScoreJson(index);
            deleteCommChoiceJson(index);
        });
    });
    $('.questionDelete').live('click', function() {
        var div = $(this).parent().parent();
        deleteDialog(function() {
            var index = div.attr('index');
            div.remove();
            json.questions.splice(index, 1);
        });
    });

    /**
     * 交换
     */
    var exchange = function(div, swap) {
        var a = div.attr('index');
        var b = swap.attr('index');
        div.attr('index', b);
        swap.attr('index', a);

        var temp = json.questions[a];
        json.questions[a] = json.questions[b];
        json.questions[b] = temp;

        temp = calc.question[a];
        calc.question[a] = calc.question[b];
        calc.question[b] = temp;
        save();
    };

    //移动问题
    $('.questionUp').live('click', function() {
        var div = $(this).parent().parent();
        var swap = div.prev('.test_panel');
        if (!swap.length) {
            return;
        }
        div.after(swap);
        exchange(div, swap);
    });
    $('.questionDown').live('click', function() {
        var div = $(this).parent().parent();
        var swap = div.next('.test_panel');
        if (!swap.length) {
            return;
        }
        div.before(swap);
        exchange(div, swap);
    });

    //移动选项分组
    $('.groupUp').live('click', function() {
        var div = $(this).parent().parent().parent();
        var swap = div.prev('.choiceGroup');
        if (!swap.length) {
            return;
        }
        div.after(swap);
    });
    $('.groupDown').live('click', function() {
        var div = $(this).parent().parent().parent();
        var swap = div.next('.choiceGroup');
        if (!swap.length) {
            return;
        }
        div.before(swap);
    });
    $('.groupDelete').live('click', function() {
        var div = $(this).parent().parent().parent();
        div.remove();
    });

    $('.groupAdd').live('click', function() {
        var div = $(this).parent().parent().parent();
        div.after($('#chiceGroup-tmpl').tmpl());
        //TODO 整理
    });

    $('.groupAdd2').live('click', function() {
        var type = $('#choiceType');
        var div = $('#chiceGroup-tmpl').tmpl();
        type.after(div);
        //TODO 整理
    });

    //移动选项
    $('.choiceUp').live('click', function() {
        var div = $(this).parent().parent();
        var swap = div.prevAll('.dialogChoice');
        if (!swap.length) {
            return;
        }
        div.after(swap);
    });
    $('.choiceDown').live('click', function() {
        var div = $(this).parent().parent();
        var swap = div.next('.dialogChoice');
        if (!swap.length) {
            return;
        }
        div.before(swap);
    });
    $('.choiceDelete').live('click', function() {
        var div = $(this).parent().parent();
        div.remove();
    });

    $('.choiceAdd').live('click', function() {
        var div = $(this).parent().parent();
        div.after($('#chiceDialog-tmpl').tmpl());
        //TODO 整理
    });

});

/**
 * 删除公共选项
 * @param deleteIndex 要删除的索引
 * @returns
 */

function deleteCommScoreJson(deleteIndex) {
    if (deleteIndex) {
        delete calc.commChoices[deleteIndex]; //删除原来的key
    }
}

/**
 * 设置分数
 * @param key
 * @param value 如果value为
 * @param deleteIndex 要删除的索引
 * @returns
 */

function setQuestionScoreJson(key, value) {
    calc.question[key] = value;
    return value;
}
/**
 * 设置分数
 * @param key
 * @param value
 * @param deleteIndex 要删除的索引
 * @returns
 */

function setCommScoreJson(key, value, deleteIndex) {
    if (deleteIndex) {
        delete calc.commChoices[deleteIndex]; //删除原来的key
    }
    if (!calc.commChoices) {
        calc.commChoices = {};
    }
    calc.commChoices[key] = value;
    return value;
}

/**
 * 取得公共选项的json
 * @param key
 * @returns {___anonymous5822_5823}
 */
function getCommChoiceJson(key) {
    var data = json.commChoices[key];
    var temp = $.extend({}, data);

    //temp.choiceGroup = data.choiceGroup;
    //temp.type = data.type;//"SINGLE";//默认

    temp.isComm = true;
    temp.index = key;

    temp.score = calc.commChoices[key];

    //temp.score.calcScript = 'if';//这里是计算脚本
    return temp;
}

/**
 * 删除公共选项
 * @param deleteIndex 要删除的索引
 * @returns
 */
function deleteCommChoiceJson(deleteIndex) {
    if (deleteIndex) {
        delete json.commChoices[deleteIndex];
    }
}


/**
 * 设置公共选项
 * @param key
 * @param value
 * @param deleteIndex 要删除的索引
 * @returns
 */
function setCommChoiceJson(key, value, deleteIndex) {
    if (deleteIndex) {
        delete json.commChoices[deleteIndex];
    }
    if (!json.commChoices) {
        json.commChoices = {};
    }
    json.commChoices[key] = value;
    return value;
}

/**
 * 校验是否有使用公共答案
 */
function useingComm(key) {
    for (var i = 0; i < json.questions.length; i++) {
        if (json.questions[i].commChoice == key) {
            return true;
        }
    }
    return false;
}

/**
 * 校验是否为已有公共答案
 */
function isComm(key) {
    for (var i in json.commChoices) {
        if (i == key) {
            return true;
        }
    }
    return false;
}

function getQuestion(i) {
    var value = jQuery.extend(true, {}, json.questions[i]);
    value.index = i;
    value.isComm = false;
    value.commChoiceDesc = ""; //使用公共答案说明
    var tempscore = calc.question[i];
    if (value.commChoice) {
        value.choiceGroup = json.commChoices[value.commChoice].choiceGroup;
        value.commChoiceDesc = "(公共选项:" + tempscore.commChoice + ")";
        tempscore = calc.commChoices[value.commChoice];
        value.type = calc.commChoices[value.commChoice].type;
    }
    value.score = tempscore;

    return value;
}
/**
 * 设置问题
 * @param key
 * @param value
 * @param deleteIndex 要删除的索引
 * @returns
 */
function setQuestion(key, value) {
    json.questions[key] = value;
    return value;
}


/**
 * parse the variable question.autoFillAnswer
 */
function parseQuestion(_question) {
    var autoFillAnswer = _question.autoFillAnswer;
    autoFillAnswer = autoFillAnswer && typeof autoFillAnswer === "string" && JSON.parse(autoFillAnswer) || autoFillAnswer;
    _question.autoFillAnswer_key = autoFillAnswer && autoFillAnswer.key || "";
    _question.autoFillAnswer_value = autoFillAnswer && autoFillAnswer.value && JSON.stringify(autoFillAnswer.value) || "";
    console.log(_question)
    return _question
}

function load(question) {
    //加载答案
    if (isReload) {
        //window.location.reload();
    } else {
        $(function() {
            answer = readAnswer();
            //恢复答案
            var div = $("#con");
            for (var key in answer) {
                var value = answer[key];
                var temp = div.find('[index=' + key + ']');
                for (var k in value) {
                    var checked = temp.find('[value=' + value[k] + ']');
                    checked.trigger('click');
                }
            }
            isReload = true;
            geneScoreView();
        });
    }

    $('#con').empty();
    $('#commChoice').empty();
    //填充数据并设置标题
    if (question) {
        json = question;
        answer = readAnswer();
    }
    for (var key in json.commChoices) {
        $("#question-tmpl").tmpl(getCommChoiceJson(key)).appendTo("#commChoice");
    }
    for (var i = 0; i < json.questions.length; i++) {

        try {
            var _question = getQuestion(i);
            _question = parseQuestion(_question);
        } catch (e) {
            console.log(e)
        }

        var a = $("#question-tmpl").tmpl(_question);
        //恢复答案
        //		(function(a, i){
        //			a.ready(function(){
        //				var value = answer[i];
        //				for(var k in value){
        //					var checked = a.find('[value='+k+']');
        //					checked.trigger('click');
        //				}
        //			});
        //		})(a, i);
        a.appendTo("#con");
    }
    $('#questionTitle').attr('who', 'title').text(q.title);
    $('#questionDesc').attr('who', 'description').text(q.description);
    $('#questionGuide').attr('who', 'guide').text(q.guide);
    $('#questionCondition').attr('who', 'condition').text(q.condition);
    $('#questionWarn').attr('who', 'warn').text(q.warn);

    var images = $.extend([], q.report_image);
    images.push("");
    var parent = $('#reportDialog .modal-body');
    var temp = $('#reportTmpl');
    for (var i in images) {
        var t = images[i];
        temp.tmpl({
            value: t
        }).appendTo(parent);
    }

}

function Customer() {
    var retVal;
    var json = localStorage.getItem('Customer');
    if (json) {
        retVal = string2Json(json);
    } else {
        retVal = {
            "age": 24,
            "gender": "MALE",
            "id": 24,
            "name": "B24",
            "edu": "COLLEGE"
        };
    }
    return retVal;
}
var customer = Customer();