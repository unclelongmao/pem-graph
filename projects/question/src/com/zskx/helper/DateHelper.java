package com.zskx.helper;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {

	private static SimpleDateFormat sdf;
	
	static {
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	}

	public static String parse(Date date){
		return sdf.format(date);
	}
	
	public static String now(){
		return sdf.format(new Date());
	}
	
}
