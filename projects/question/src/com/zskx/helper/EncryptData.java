package com.zskx.helper;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.Security;

import javax.crypto.Cipher;

import org.apache.log4j.Logger;

/**
 * 提供加密算法，可以对输入的字符串进行加密、解密操作
 */
public class EncryptData {

	private static final Logger LOGGER = Logger.getLogger(EncryptData.class);

	/** 字符串默认键值 */
	private static String strDefaultKey = "national";

	private static EncryptData instance = new EncryptData();

	/** 加密工具 */
	private Cipher encryptCipher = null;

	/** 解密工具 */
	private Cipher decryptCipher = null;

	/**
	 * 将byte数组转换为表示16进制值的字符串， 如：byte[]{8,18}转换为：0813， 和public static byte[]
	 * hexStr2ByteArr(String strIn) 互为可逆的转换过程
	 * 
	 * @param arrB
	 *            需要转换的byte数组
	 * @return 转换后的字符串
	 * @throws Exception
	 *             本方法不处理任何异常，所有异常全部抛出
	 */
	private static String byteArr2HexStr(byte[] arrB) {
		int iLen = arrB.length;
		// 每个byte用两个字符才能表示，所以字符串的长度是数组长度的两倍
		StringBuffer sb = new StringBuffer(iLen * 2);
		for (int i = 0; i < iLen; i++) {
			int intTmp = arrB[i];
			// 把负数转换为正数
			while (intTmp < 0) {
				intTmp = intTmp + 256;
			}
			// 小于0F的数需要在前面补0
			if (intTmp < 16) {
				sb.append("0");
			}
			sb.append(Integer.toString(intTmp, 16));
		}
		return sb.toString();
	}

	/**
	 * 将表示16进制值的字符串转换为byte数组， 和public static String byteArr2HexStr(byte[] arrB)
	 * 互为可逆的转换过程
	 * 
	 * @param strIn
	 *            需要转换的字符串
	 * @return 转换后的byte数组
	 * @throws Exception
	 *             本方法不处理任何异常，所有异常全部抛出
	 * @author <a href="mailto:leo841001@163.com">LiGuoQing</a>
	 * @throws UnsupportedEncodingException 
	 */
	private static byte[] hexStr2ByteArr(String strIn) throws UnsupportedEncodingException {
		byte[] arrB = strIn.getBytes("UTF-8");
		int iLen = arrB.length;

		// 两个字符表示一个字节，所以字节数组长度是字符串长度除以2
		byte[] arrOut = new byte[iLen / 2];
		for (int i = 0; i < iLen; i = i + 2) {
			String strTmp = new String(arrB, i, 2);
			arrOut[i / 2] = (byte) Integer.parseInt(strTmp, 16);
		}
		return arrOut;
	}

	/**
	 * 默认构造方法，使用默认密钥
	 * @throws UnsupportedEncodingException 
	 * 
	 * @throws Exception
	 */
	public EncryptData() {
		this(strDefaultKey);
	}

	/**
	 * 指定密钥构造方法
	 * 
	 * @param strKey
	 *            指定的密钥
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public EncryptData(String strKey) {
		Security.addProvider(new com.sun.crypto.provider.SunJCE());
		try {
			Key key = getKey(strKey.getBytes("UTF-8"));
			encryptCipher = Cipher.getInstance("DES");
			encryptCipher.init(Cipher.ENCRYPT_MODE, key);

			decryptCipher = Cipher.getInstance("DES");
			decryptCipher.init(Cipher.DECRYPT_MODE, key);
		} catch (Exception e) {
			LOGGER.error("EncryptData error", e);
		}

	}

	/**
	 * 加密字节数组
	 * 
	 * @param arrB
	 *            需加密的字节数组
	 * @return 加密后的字节数组
	 * @throws Exception
	 */
	private byte[] encrypt(byte[] arrB) {
		try {
			return encryptCipher.doFinal(arrB);
		} catch (Exception e) {
			LOGGER.error("decrypt error", e);
			return null;
		}
	}

	/**
	 * 加密字符串
	 * 
	 * @param strIn
	 *            需加密的字符串
	 * @return 加密后的字符串
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public String encrypt(String strIn) {
		if (strIn == null) {
			return null;
		}
		try {
			return byteArr2HexStr(encrypt(strIn.getBytes("UTF-8")));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 解密字节数组
	 * 
	 * @param arrB
	 *            需解密的字节数组
	 * @return 解密后的字节数组
	 * @throws Exception
	 */
	private byte[] decrypt(byte[] arrB) {
		try {
			return decryptCipher.doFinal(arrB);
		} catch (Exception e) {
			LOGGER.error("decrypt error", e);
			return null;
		}
	}

	/**
	 * 解密字符串
	 * 
	 * @param strIn
	 *            需解密的字符串
	 * @return 解密后的字符串
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public String decrypt(String strIn) throws UnsupportedEncodingException {
		if (strIn == null) {
			return null;
		}

		return new String(decrypt(hexStr2ByteArr(strIn)), "UTF-8");
	}

	/**
	 * 从指定字符串生成密钥，密钥所需的字节数组长度为8位 不足8位时后面补0，超出8位只取前8位
	 * 
	 * @param arrBTmp
	 *            构成该字符串的字节数组
	 * @return 生成的密钥
	 * @throws java.lang.Exception
	 */
	private Key getKey(byte[] arrBTmp) {
		// 创建一个空的8位字节数组（默认值为0）
		byte[] arrB = new byte[8];

		// 将原始字节数组转换为8位
		for (int i = 0; i < arrBTmp.length && i < arrB.length; i++) {
			arrB[i] = arrBTmp[i];
		}

		// 生成密钥
		Key key = new javax.crypto.spec.SecretKeySpec(arrB, "DES");

		return key;
	}
	
	public static EncryptData instance(){
		return instance;
	}
	

	public static void main(String[] args) {
		String xmlContent = "<id='question'>数据安全保护<id>asda<id='question'>数据安全保ion'>数据安全保护<id>asda<id='question'>数据安全保护<护<id><id='question'>"
				+ "数据安全保护<id><iddas='question'>数据dasds安全保护<id>安全保ion'>数据安全保护<id>asda<id='question'>数据安全保护<护<id><id='question'>";

		try {
			EncryptData encryptData = instance();// 自定义密钥
			System.out.println("加密前的字符：" + xmlContent + "length:" + xmlContent.length());
			long start = System.currentTimeMillis();
			String encryptResult = encryptData.encrypt(xmlContent);
			System.out.println("加密后的字符：" + encryptResult + "length:" + xmlContent.length());

			long end = System.currentTimeMillis();
			System.out.println(end - start);

			String decryptResult = encryptData.decrypt(encryptResult);
			System.out.println("解密后的字符：" + decryptResult);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}