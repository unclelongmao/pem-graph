package com.zskx.service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class QuestionCalcService {
	
	private static Invocable jsFunction = null;

	static {
		resetJsInvocable();
	}
	
	private static void loadResource(ScriptEngine engine, String resource){
		// Run InvScript.js
		Reader scriptReader = new InputStreamReader(QuestionCalcService.class.getResourceAsStream(resource));
		try {
			engine.eval(scriptReader);
		}catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				scriptReader.close();
			} catch (IOException e) {
			}
		}
	}
	
	public static void resetJsInvocable() {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		loadResource(engine, "/json2.js");
		loadResource(engine, "/calc.js");
		if (engine instanceof Invocable) {
			jsFunction = (Invocable) engine;
		}else{
			throw new RuntimeException("not suppont!");
		}
	}
    
    
    /**
     * 取得计算因子
     * @param id
     * @param version
     * @return
     */
    private static String getGene(int id, Double version){
    	Map<String, Object> map = JdbcUtils.queryOne("SELECT gene from question_t where id=? and version=?", id, version);
    	return map.get("gene").toString();
    }
    
    /**
     * 取得计算脚本
     * @param id
     * @param version
     * @return
     */
    private static String getCalc(int id, Double version){
    	Map<String, Object> map = JdbcUtils.queryOne("SELECT calc from question_t where id=? and version=?", id, version);
    	System.out.println(map);
    	return map.get("calc").toString();
    }
	
    /**
     * 计算问题的原始分
     * @param calc 计算脚本(jons)
     * @param answer 返回答案(jons)
     * @return
     */
    public static List<Double> calcQuestionScore(String calc, String answer, String user){
    	List<Double> list = new ArrayList<Double>();
    	try {
			jsFunction.invokeFunction("getQuestionScore", calc, answer, list, user);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return list;
    }
    
    /**
     * 计算问题的原始分
     * @param id 量表id
     * @param answer 返回答案(jons)
     * @return
     */
    public static List<Double> calcQuestionScore(int id, Double version, String answer, String user){
    	return calcQuestionScore(getCalc(id, version), answer, user);
    }
    
    /**
     * 计算因子分数
     * @param gene 因子脚本
     * @param questionScore 问题分数
     * @return
     */
    public static List<Map<String, Double>> calcGeneScore(String gene, String questionScore, String user){
    	List<Map<String, Double>> list = new ArrayList<Map<String,Double>>();
    	try {
    		jsFunction.invokeFunction("getGeneScore", gene, questionScore, list, user);
    	} catch (Exception e) {
    		throw new RuntimeException(e);
    	}
    	return list;
    }
    
    public static List<Map<String, Double>> calcGeneScore(int id, Double version, String scoreList, String user){
    	return calcGeneScore(getGene(id, version), scoreList, user);
    }
    
    /**
     * 取得评语
     * @param gene 因子脚本
     * @param geneScore 因子分数
     * @return
     */
    public static Map<String, Map<String, String>> getAppraise(String gene, String geneScore, String warn){
    	Map<String, Map<String, String>> map = new LinkedHashMap<String, Map<String, String>>();
    	try {
    		jsFunction.invokeFunction("getAppraiseJava", gene, geneScore, map, warn);
    	} catch (Exception e) {
    		throw new RuntimeException(e);
    	}
    	return map;
    }
    
    /**
     * 取得评语
     * @param id
     * @param version
     * @param geneScore
     * @return
     */
    public static Map<String, Map<String, String>> getAppraise(int id, Double version, String geneScore, String warn){
    	Map<String, Object> map = JdbcUtils.queryOne("SELECT gene, warn from question_t where id=? and version=?", id, version);
    	if(warn==null || "".equals(warn)){
    		warn = map.get("warn").toString();
    	}
    	return getAppraise(map.get("gene").toString(), geneScore, warn);
    }
    
    /**
     * 辅助方法, 转为json
     * @param o
     * @return
     */
    public static String toJson(Object o){
    	return JSON.toJSONString(o, SerializerFeature.WriteDateUseDateFormat);
    }
    
    public static void main(String[] args) {
    	String answer = "[['B'],['B'],['B'],['A'],['A'],['B'],['A'],['A'],['B'],['B'],['A'],['B'],['A'],['A'],['A'],['A'],['A'],['A'],['B'],['A'],['A'],['B'],['A'],['A'],['A'],['B'],['B'],['A']]";
		int id = 4002;
		Double version = 0.003;
		String user = "{'age':25,'gender':'MALE','id':25,'name':'B25','edu':'COLLEGE'}";
		String warn = "{'F2':[{'max':999,'min':-999,'content':'h'}]}";
		List<Double> list = calcQuestionScore(id , version , answer, user);
    	List<Map<String, Double>> geneScore = calcGeneScore(id, version, list.toString(), user);
		Map<String, Map<String, String>> appraise = getAppraise(id, version, toJson(geneScore), warn);
    	System.out.println(appraise);
    	System.out.println(toJson(appraise));
	}
    
}
