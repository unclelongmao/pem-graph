package com.zskx.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 2011-11-3 下午08:15:02
 * @author <a href="mailto:gaollg@sina.com">Gaollg</a>
 */
public class JdbcUtils {
	
	private static String className = "com.mysql.jdbc.Driver";
	/*
	private static String url = "jdbc:mysql://localhost:3306/plain2?useUnicode=true&characterEncoding=UTF-8";
	private static String user = "root";
	private static String password = "root";
	*/
	static {
		try {
			Class.forName(className);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	

	/**
	 * 禁止实例化
	 */
	private JdbcUtils() {}

	/**
	 * 取得连接
	 * @param url 
	 * @param username 
	 * @param password 
	 * @return
	 * @throws SQLException 取得连接失败时抛出异常
	 */
	public static Connection getConnection(String url, String username, String password){
		Connection conn = null;
		
		try {
			conn = DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}
		return conn;
	}
	
	public static Connection getConnection(){
		return getConnection("jdbc:mysql://127.0.0.1/question_work", "root", "123456");
	}
	
	/**
	 * 关闭连接,释放资源
	 * @param rs
	 * @param stmt
	 * @param conn
	 */
	public static void free(ResultSet rs, Statement stmt, Connection conn) {
		try { // 捕捉异常
			try {
				if (rs!=null) { // 当ResultSet对象的实例rs不为空时
					rs.close(); // 关闭ResultSet对象
				}
			} finally {
				try {
					if (stmt!=null) { // 当Statement对象的实例stmt不为空时
						stmt.close(); // 关闭Statement对象
					}
				}catch (Exception e) {
					e.printStackTrace();
				} 
				finally {
					if (conn!=null) { // 当Connection对象的实例conn不为空时
						conn.close(); // 关闭Connection对象
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace(); // 输出异常信息
		}
	}

	/**
	 * 给PreparedStatement赋值
	 * @param pstmt
	 * @param values
	 * @throws SQLException
	 */
	private static void setPstmtValues(PreparedStatement pstmt, Object... values)
			throws SQLException {
		if(pstmt!=null&values!=null){
			for (int i = 0; i < values.length; i++) {
				pstmt.setObject(i+1, values[i]);
			}
		}
	}
	
	/**
	 * 执行SQL语句(insert,update,delete)
	 * @param callBack 回调函数中rs可获取[由于执行此 Statement 对象]而创建的所有自动生成的键
	 * @param sql sql语句
	 * @param values 对就占位符的值
	 * @return 返回影响的行数
	 */
	public static int execute(JdbcUtils.CallBack callBack, String sql,Object... values){
		int retVal = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			conn = JdbcUtils.getConnection();
			pstmt = conn.prepareStatement(sql);
			setPstmtValues(pstmt, values);
			retVal = pstmt.executeUpdate();
			if(callBack!=null){
				rs = pstmt.getGeneratedKeys();
				callBack.callback(rs);//回调
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}finally{
			JdbcUtils.free(rs, pstmt, conn);
		}
		return retVal;
	}
	
	/**
	 * 执行SQL语句(insert,update,delete)
	 * @param sql sql语句
	 * @param values 对就占位符的值
	 * @return 返回影响的行数
	 */
	public static int execute(String sql,Object... values){
		return execute(null, sql, values);
	}
	
	/**
	 * 回调一次 未经过任何处理的ResultSet
	 * @param callBack
	 * @param sql
	 * @param values
	 * @see JdbcUtils.query()
	 */
	public static void query(JdbcUtils.CallBack callBack, String sql, Object... values){
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			conn = JdbcUtils.getConnection();
			pstmt = conn.prepareStatement(sql);
			setPstmtValues(pstmt, values);
			rs = pstmt.executeQuery();
			callBack.callback(rs);
		}catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}finally{
			JdbcUtils.free(rs, pstmt, conn);
		}
	}
	
	/**
	 * JdbcUtils回调接口
	 * 2011-11-3 下午08:29:15
	 * @author <a href="mailto:gaollg@sina.com">Gaollg</a>
	 *
	 */
	public static interface CallBack{
		/**
		 * 回调方法
		 * @throws SQLException 
		 */
		public void callback(ResultSet rs) throws SQLException;
	}
	
	/**
	 * 查询实体列表
	 * @param <T> 
	 * @param sql
	 * @param values
	 * @return
	 */
	public static List<Map<String, Object>> queryList(String sql, Object... values){
		final List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		JdbcUtils.query(new JdbcUtils.CallBack() {
			public void callback(ResultSet rs) throws SQLException {
				//取得列名
				ResultSetMetaData data = rs.getMetaData();
				/** 数据库中的列  */
				List<String> dataColumns = new ArrayList<String>();
				for(int i=0;i<data.getColumnCount();i++){
					dataColumns.add(data.getColumnLabel(i+1));
				}
				//循环并注入
				while(rs.next()){
					Map<String, Object> map = new HashMap<String, Object>();
					try {
						for(String key : dataColumns){
							map.put(key, rs.getObject(key));
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
					list.add(map);
				}
			}
			
		}, sql, values);
		return list;
	}
	
	/**
	 * 查询实体
	 * @param <T>
	 * @param sql
	 * @param values
	 * @return
	 */
	public static Map<String, Object> queryOne(String sql, Object... values){
		List<Map<String, Object>> list = queryList(sql, values);
		if(list.size()==0){
			return null;
		}
		return list.get(0);
	}
	
	/**
	 * 查询个数
	 * @param sql
	 * @param values
	 * @return
	 */
	public static long queryCount(String sql, Object... values) {
		final List<Long> list = new ArrayList<Long>();
		JdbcUtils.query(new JdbcUtils.CallBack() {
			public void callback(ResultSet rs) throws SQLException {
				if(rs.next()){
					list.add(rs.getLong(1));
				}
			}
		}, sql, values);
		return list.get(0);
	}
	
}
