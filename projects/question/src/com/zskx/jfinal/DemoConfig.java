package com.zskx.jfinal;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jsmart.bigpipe.BigPipeHelper;
import com.jsmart.bigpipe.BigpipeHandler;
import com.jsmart.bigpipe.SmartCode;
import com.jsmart.validators.JsValidatorInterceptor;
import com.zskx.action.KindAction;
import com.zskx.action.QuestionAction;
import com.zskx.action.QuestionVersionAction;
import com.zskx.action.UserAction;
import com.zskx.domain.Question;
import com.zskx.domain.TypicalCustomerAnswer;

/**
 * API引导式配置
 */
public class DemoConfig extends JFinalConfig {
	
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		// 加载少量必要配置，随后可用getProperty(...)获取值
		loadPropertyFile("a_little_config.txt");
		me.setDevMode(getPropertyToBoolean("devMode", false));
		SmartCode s = new SmartCode();
		s.setCode(-5);
		s.setMsg("sssss");
		s.setUrl("http://www.baidu.com");
		BigPipeHelper.add(s);
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add("/question", QuestionAction.class);
		me.add("/kind",KindAction.class);
		me.add("/user",UserAction.class);
		me.add("/questionVersion",QuestionVersionAction.class);
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		// 配置C3p0数据库连接池插件
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password").trim());
		me.add(c3p0Plugin);
		
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		me.add(arp);
		arp.addMapping("question_t", Question.class);
		arp.addMapping("typical_customer_answer_t", TypicalCustomerAnswer.class);
	}
	
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		me.add(new JsValidatorInterceptor());
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		me.add(new BigpipeHandler());
		BigPipeHelper.setCommInfo(new BigPipeHelper.ConfigComm() {
			@Override
			public void configComm(Map<String, Object> map, HttpServletRequest request,
					HttpServletResponse response) {
				map.put("a", "b");
				map.put("b", new Question());
			}
		});
		//BigPipeHelper.setIMAGEBase("file:///home/gaolei/workspacePolice/app03");
	}
	
	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		JFinal.start("WebRoot", 8006, "/", 5);
		//JFinal.start("/home/gaolei/workspaceJFinal/znf-demo/znf-demo-deploy/target/ROOT/WEB-INF", 8006, "/", 5);
	}
	
}
