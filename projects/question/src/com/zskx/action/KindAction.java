package com.zskx.action;

import java.util.Date;
import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jsmart.controller.SmartController;

public class KindAction extends SmartController {

	public void addKind() {

		String id = getPara("id");
		String kindName = getPara("kindName");
		String parentKindId = getPara("parentKindId");
		if (parentKindId == null || "".equals(parentKindId)) {
			parentKindId = "0";
		}
		try {
			Record record = new Record().set("kind_name", kindName).set("parent_kind_id", parentKindId)
					.set("create_at", new Date()).set("removed", "N").set("id", id);
			Db.save("question_kind_t", record);
			this.putUsefulData("msg", "success");
		} catch (Exception ex) {
			this.putUsefulData("msg", "error");
		}

	}

	public void findAll() {
		List<Record> list = Db
				.find("SELECT id,kind_name,q_index FROM question.question_kind_t where removed='N' order by q_index");
		this.putUsefulData("data", list);
	}

	public void update() {
		String id = getPara("id");
		String kindName = getPara("kindName");
		try {
			Db.update("update question_kind_t set kind_name='" + kindName + "', modify_at=sysdate()  where id=" + id);
			this.putUsefulData("msg", "success");
		} catch (Exception ex) {
			this.putUsefulData("msg", "error");
		}

	}

	public void addQuestion() {
		String id = getPara("id");
		String questionName = getPara("questionName");
		String kindId = getPara("kindId");
		try {

			Record record = new Record().set("title", questionName).set("question_kind_id", kindId)
					.set("create_at", new Date()).set("removed", "N").set("id", id)
					.set("question", "").set("gene", "").set("calc", "");
			Db.save("question_t", record);
			this.putUsefulData("msg", "success");
		} catch (Exception ex) {
			this.putUsefulData("msg", "error");
		}

	}

}
