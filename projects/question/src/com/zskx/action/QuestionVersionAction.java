package com.zskx.action;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jsmart.controller.SmartController;

public class QuestionVersionAction extends SmartController {

	public void questionList() {
		String id = getPara(0);
		List<Record> list = Db.find("select id,version,modify_reason,title,create_at,modify_at from question_t where removed='N' and id ="
						+ id + " order by version desc");
		this.putUsefulData("data", list);
	}

	public void findUserTest() {
		List<Record> list = Db.find("select id,name,age,gender,intro from typical_customer_t");
		String questionId = getPara("questionId");
		String version = getPara("version").replace("_", ".");
		
		List<Record> newList = new ArrayList<Record>();
		for (Record record : list) {
			int id = record.getInt("id");

			List<Record> children = Db.find("select an.id,an.typical_customer_id,an.question_id,an.version,an.answer,an.create_at,us.mail,us.id user_id,an.intro  from typical_customer_answer_t an left join user_t us on an.create_id=us.id where an.typical_customer_id ="
							+ id
							+ " and an.question_id ="
							+ questionId
							+ " and an.version="
							+ version
							+ " order by an.create_at desc");

			record.put("children", children);
			if (children.size() != 0) {
				newList.add(record);
			}
		}

		this.putUsefulData("data", newList);

		//查找  测试版本
		Record question = Db.findFirst("SELECT id,title FROM question_t where id=" + questionId + "  and version="
				+ version);

		this.putUsefulData("question", question);

	}

}
