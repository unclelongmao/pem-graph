var length = json.appraise.length
var datas = [];
var geneName = [];
var label = {};
var index = 0;
for (var i = 0; i < json.appraise.length; i++) {
    json.appraise[i].score && !/all/g.test(json.appraise[i].key) && datas.push(json.appraise[i].score)
    json.appraise[i].score && !/all/g.test(json.appraise[i].key) && json.appraise[i].title && geneName.push(json.appraise[i].title.substr(0, 1))
}

window.hideNodeText = ["0", "20", "30"];

var config = {
    chart: {
        polar: true,
        type: "line"
    },
    title: {
        text: "",
        x: -80
    },
    pane: {
        size: "80%"
    },
    xAxis: {
        tickmarkPlacement: "on",
        lineWidth: 0,
        labels: {
            formatter: function() {
                var temp = label[this.value];
                if (!temp) {
                    temp = geneName[index];
                    label[this.value] = temp;
                    index++;
                }
                return temp;
            }
        }
    },
    yAxis: {
        gridLineInterpolation: "polygon",
        gridLineDashStyle: 'longdash',
        gridLine_custom: [{
            index: 1,
            gridLineWidth: 1,
            gridLineColor: "#f00",
            gridLineDashStyle: "solid"
        }, {
            index: 4,
            gridLineWidth: 1,
            gridLineDashStyle: "solid",
            gridLineColor: "#C0C0C0",
        }],
        tickPositions: [0, 10, 20, 30, 40],
        showLastLabel_custom: true,
        lastLabelIndex: 40,
        lineWidth: 0,
    },
    tooltip: {
        formatter: function() {
            var str = label[this.x] + ': ' + this.y + '分';
            if (0 && showPanel) {
                str = '<b>' + this.series.name + '</b><br/>' + str;
            }
            return str;
        }
    },
    legend: {
        enabled: false
    },
    series: [{
        type: 'area',
        data: datas,
        pointPlacement: "on",
        color: '#7fa9fb'
    }],
};

image(['F1', 'F2', 'F3', 'F4'], 'spider', config)