var length = json.appraise.length
var datas = [];
var geneName = [];
for (var i = 0; i < json.appraise.length; i++) {
    json.appraise[i].score && datas.push(json.appraise[i].score)
    json.appraise[i].score && json.appraise[i].title && geneName.push(json.appraise[i].title)
}

geneName.splice(4, 0, "splitter_node");
datas.splice(4, 0, null);

var config = {
    xAxis: {
        categories: geneName,
        labels: {
            rotation: 55,
            align: 'left',
            style: {
                fontSize: '11px',
                fontFamily: 'Verdana, sans-serif',
                marginLeft: '-10px',
            },
            x: -20
        },
    },
    yAxis: {
        tickInterval: 20,
        max: 140,
        min: 0,
    },
    series: [{
        data: datas
    }]
};
image(['L', 'F', 'K', 'Hs', 'D', 'Hy', 'Pd', 'Mf', 'Pa', 'Pt', 'Sc', 'Ma', 'Si'], 'line', config)