var length = json.appraise.length
var datas = [];
var geneName = [];
for (var i = 0; i < json.appraise.length; i++) {
    json.appraise[i].score && datas.push(json.appraise[i].score)
    json.appraise[i].score && json.appraise[i].title && geneName.push(json.appraise[i].title)
}

geneName.splice(3, 0, "splitter_node");
datas.splice(3, 0, null);

var config = {
    chart: {
        marginRight: 45
    },
    isxAxisZero: true,
    categories: geneName,
    xAxis: {
        labels: {
            enabled: true,
            formatter: function() {
                return geneName[this.value];
            },
            rotation: 55,
            align: 'left',
            style: {
                fontSize: '11px',
                fontFamily: 'Verdana, sans-serif',
                marginLeft: '-10px',
            },
            x: -5,
        },
        tickInterval: 1,
        minPadding: 0,
        maxPadding: 0,
        startOnTick: true,
        endOnTick: true
    },
    yAxis: {
        tickInterval: 20,
        max: 140,
        min: 0,
    },
    series: [{
        allowPointSelect: true,
        data: datas
    }]
};
image(['L', 'F', 'K', 'Hs', 'D', 'Hy', 'Pd', 'Mf', 'Pa', 'Pt', 'Sc', 'Ma', 'Si'], 'line', config)