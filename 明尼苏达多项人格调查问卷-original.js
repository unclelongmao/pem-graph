var config = {
    xAxis: {
        labels: {
            staggerLines: 2
        }
    },
    yAxis: {
        tickInterval: 20,
        max: 140,
        min: 0,
    }
};
image(['L', 'F', 'K', 'Hs', 'D', 'Hy', 'Pd', 'Mf', 'Pa', 'Pt', 'Sc', 'Ma', 'Si'], 'line', config)