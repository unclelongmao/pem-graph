var length = json.appraise.length
var datas = [];
var geneName = [];
var label = {};
var index = 0;
for (var i = 0; i < json.appraise.length; i++) {
    json.appraise[i].score && !/all/g.test(json.appraise[i].key) && datas.push(json.appraise[i].score)
    json.appraise[i].score && !/all/g.test(json.appraise[i].key) && json.appraise[i].title && geneName.push(json.appraise[i].title)
}

window.hideNodeText = ["0"]

var config = {
    chart: {
        polar: true,
        type: "line"
    },
    title: {
        text: "",
        x: -80
    },
    pane: {
        size: "80%"
    },
    xAxis: {
        tickmarkPlacement: "on",
        lineWidth: 0,
        gridLineColor: '#ff9906',
        gridLineWidth: 4,
        labels: {
            formatter: function() {
                var temp = label[this.value];
                if (!temp) {
                    console.log(geneName)
                    temp = geneName[index];
                    label[this.value] = temp;
                    index++;
                }
                return temp;
            }
        }
    },
    yAxis: {
        gridLineInterpolation: "polygon",
        gridLineDashStyle: 'longdash',
        gridLine_custom: [{
            index: 4,
            gridLineWidth: 4,
            gridLineDashStyle: "solid",
            gridLineColor: "#ff9906"
        }],
        tickPositions: [0, 10, 20, 30, 44],
        showLastLabel_custom: true,
        lastLabelIndex: 44,
        lineWidth: 0,
    },
    tooltip: {
        formatter: function() {
            var str = label[this.x] + ': ' + this.y + '分';
            if (0 && showPanel) {
                str = '<b>' + this.series.name + '</b><br/>' + str;
            }
            return str;
        }
    },
    legend: {
        enabled: false
    },
    series: [{
        type: 'area',
        data: datas,
        pointPlacement: "on",
        color: '#7fa9fb'
    }],
};

image(['F1', 'F2', 'F3', 'F4', 'F5', 'F6'], 'spider', config)